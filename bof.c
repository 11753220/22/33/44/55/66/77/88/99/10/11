#include <stdio.h>
#include <string.h>

void vulnerableFunction(char *input) {
    char buffer[8];
    strcpy(buffer, input);
    printf("Buffer contents: %s\n", buffer);
}

int main() {
    char input[16];
    printf("Enter a string: ");
    fgets(input, sizeof(input), stdin);
    vulnerableFunction(input);
    return 0;
}
